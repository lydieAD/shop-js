//si le document et bien chargé dans la totalité, on déclenche les event listener

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready()
}

function ready() {

//variable du bouton supprimer

    let button;
    let i;
    let removeCartItemButtons = document.getElementsByClassName('btn-danger')
    for (i = 0; i < removeCartItemButtons.length; i++) {
        button = removeCartItemButtons[i];
        button.addEventListener('click', removeCartItem)
    }

//variable pour modifier la quantité avec curseur mais voir
// variable quantityChanged pour valeur negative impossible
    let quantityInputs = document.getElementsByClassName('cart-quantity-input');

    for (i = 0; i < quantityInputs.length; i++) {
        const input = quantityInputs[i];
        input.addEventListener('change', quantityChanged)
    }
//variable ajouter au panier avec le bouton
    let addToCartButtons = document.getElementsByClassName('shop-item-button')
    for (i = 0; i < addToCartButtons.length; i++) {
        button = addToCartButtons[i];
        button.addEventListener('click', addToCartClicked)
    }

    document.getElementsByClassName('btn-purchase')[0].addEventListener('click', purchaseClicked)
}
//installation de stripe //configuration de stripe
 let stripeHandeler =
     StripeCheckout.configure({
     key:stripePublicKey,
     locale : 'fr',
     token : function (token){
         let item = []
         let cartItemContainer = document.getElementsByClassName('cart-items')[0]
         let cartRows =cartItemContainer.getElementsByClassName('cart-row')
         for (let i = 0; i < cartRows.length; i++){
             let cartRow = cartRows [i]
             let quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]
             let quantity = quantityElement.valueOf()
             let id = cartRow.dataset.itemId


             items.push({
                 id: id,
                 quantity : quantity

             })
         }
         ///rafraichir la page

         fetch('/continuer',{
             method : 'POST',
             headers : {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json'
                 } ,
             body: JSON.stringify({
                 stripeTokenId : token.id,
                 items: items
             })
         }).then(function(res){
             return res.json()
         }).then(function(data){
             alert('data.message')
             let cartItems = document.getElementsByClassName('cart-items')[0]
             while (cartItems.hasChildNodes()) {
                 cartItems.removeChild(cartItems.firstChild)
             }

             //mettre le total à zéro
             updateCartTotal()



         }).catch(function(error){
             console.error(error)
         })


     }


 })



//passer à la caisse
function purchaseClicked() {
    //avant installation de STRIPE
    // alert('Passer à la caisse')
    // let cartItems = document.getElementsByClassName('cart-items')[0]
    // while (cartItems.hasChildNodes()) {
    //     cartItems.removeChild(cartItems.firstChild)
    // }
    //
    // //mettre le total à zéro
    // updateCartTotal()

    let priceElement = document.getElementsByClassName('cart-total-price')[0]
    let price = parseFloat (priceElement.innerText.replace('€',''))
 stripeHandeler.open({
     amount: price
 })

}
//fonction pour rendre le code de la fonction removeCartItemButtons plus clair

// button.addEventListener('click', function(event){
//let buttonClicked.parentElement.parentElement.remove()updateCartTotal()
//})
function removeCartItem(event) {
    let buttonClicked = event.target
    buttonClicked.parentElement.parentElement.remove()
    updateCartTotal()
}
//valeur negative impossible et on peut saisir un nombre
function quantityChanged(event) {
    let input = event.target
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
}

//fonction ajouter au panier
//ici on veux que l'image de l'article le nom et le prix soit ajouter
//on va chercher chaque élément dans le html

function addToCartClicked(event) {
    let button = event.target
    let shopItem = button.parentElement.parentElement
    let title = shopItem.getElementsByClassName('shop-item-title')[0].innerText
    let price = shopItem.getElementsByClassName('shop-item-price')[0].innerText
    let imageSrc = shopItem.getElementsByClassName('shop-item-image')[0].src
    //stripe
    let id = shopItem.dataset.itemId
    //
    addItemToCart(title, price, imageSrc)
    updateCartTotal()
}
//fonction ajouter avec mise en page//id apres ajout de stripe
function addItemToCart(title, price, imageSrc, id) {
//par exemple le nom à coté de l'image
    let cartRow = document.createElement('div')
    cartRow.classList.add('cart-row')

    //stripe
    cartRow.dataset.itemId =id


//ici on cherche à ne pas créer deux ligne si article similaire
    let cartItems = document.getElementsByClassName('cart-items')[0]
    let cartItemNames = cartItems.getElementsByClassName('cart-item-title')
    for (let i = 0; i < cartItemNames.length; i++) {

 //ici un message pop up pour informer que l'article existe deja
        if (cartItemNames[i].innerText === title) {
            alert('cet article est déjà ajouté au panier ')
            return
        }
    }

//variable pour ajouter au panier mais avec tout les élément de l'article repris dans le html
    cartRow.innerHTML = `
        <div class="cart-item cart-column">
            <img class="cart-item-image" src="${imageSrc}" width="100" height="100">
            <span class="cart-item-title">${title}</span>
        </div>
        <span class="cart-price cart-column">${price}</span>
        <div class="cart-quantity cart-column">
            <input class="cart-quantity-input" type="number" value="1">
            <button class="btn btn-danger" type="button">REMOVE</button>
        </div>`
    cartItems.append(cartRow)
    cartRow.getElementsByClassName('btn-danger')[0].addEventListener('click', removeCartItem)
    cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', quantityChanged)
}




//fonction permettant de mettre à jour la partie total du panier
//par cart article
function updateCartTotal() {
    let cartItemContainer = document.getElementsByClassName('cart-items')[0]
    let cartRows = cartItemContainer.getElementsByClassName('cart-row')
    let total = 0
    for (let i = 0; i < cartRows.length; i++) {
        let cartRow = cartRows[i]
 //par colonne prix

        let priceElement = cartRow.getElementsByClassName('cart-price')[0]

////par quantité

        let quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]

//calcul total en fonction de la quantité

        let price = parseFloat(priceElement.innerText.replace('€', ''))
        let quantity = quantityElement.value
        total = total + (price * quantity)
    }
//pour affiche un prix à deux chifre apres la virgule
    total = Math.round(total * 100) / 100
    document.getElementsByClassName('cart-total-price')[0].innerText = '€' + total
}